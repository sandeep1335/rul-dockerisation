# -*- coding: utf-8 -*-
"""
Created on Mon Jul 27 08:14:15 2020

@author: mohammads6
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os 
import glob
import sys

def extract_cycle_info_by_viscosity(df):
    """
    This function extract the oil cycle information based on viscosity.
    Here we are considering that every oil cycle start with viscosity greater 
    than 99% and based on that we are calculating cycle information.        
    """
    
    #Changing the column name as sorting the data based on given timestamp
    df['time_stamp'] = pd.to_datetime(df['time_stamp'])
    df.sort_values('time_stamp',inplace=True)
    df.reset_index(inplace=True)
    
    from itertools import groupby, count
    oil_cycle = []
    df['oil_cycle'] = 0
    cycle_index = df[df['viscosity_until_serv']>99].index
    cycle=[]
    
    for index in range(len(cycle_index)):
        if index==0:
            cycle.append(cycle_index[index])
        else:
            if cycle_index[index] -cycle_index[index-1]>50000:
                cycle.append(cycle_index[index])
    count =1
    for index in range(len(cycle)):
        if index ==0:
            df['oil_cycle'].iloc[0:cycle[index]] = count
        else:
            df['oil_cycle'].iloc[cycle[index-1]:cycle[index]] = count

        count+=1    
    
    df['oil_cycle'][df['oil_cycle']==0] = df['oil_cycle'].max()+1
    if len(df[df['oil_cycle']==1])<20000:
        df['oil_cycle'][df['oil_cycle']==1]=2
        df['oil_cycle']= df['oil_cycle']-1
        
    return df

def prepare_data(df):
    """
    This function extract the engine start status and calculate the acceleration-decceleration of the vehicle
    """
    df['start_status'] = np.where(df['time_vector (ms)'] ==0, 1, 0)
    ## Removing the rows where gps data is missing and dorping the duplicate column values.

    
    # Calculating the acceleration and decceleration of the vehicle
    df['speed_difference'] = df['vehicle_speed'].diff()
    df['time_stamp_difference'] = df['time_stamp'].diff()
    df['time_stamp_difference'] = df['time_stamp_difference']/np.timedelta64(1, 's')
    df['acceleration-decceleration'] = df['speed_difference']/df['time_stamp_difference']    
    
    return df


#
def create_chunk_by_km(df,km=100):
    """
    Create list of data chunk
    """
    
    chunk_dfs= []
    
    df['odometer_new'] = np.where(df['odometer']==-0.12345,np.nan, df['odometer'])
    df['odometer_new'] = df['odometer_new'].fillna(method='bfill')
    cycles = df['oil_cycle'].unique()
    
    for cycle in cycles:
        c_df = df[df['oil_cycle']==cycle]       
        c_df.reset_index(inplace=True,drop=True)
        
        if(c_df.shape[0]>0):
            
            data_range =  np.arange(c_df['odometer_new'].min(),c_df['odometer_new'].max(),km)
            if len(data_range) >1:
                index_range =pd.cut(c_df['odometer_new'],data_range,include_lowest=True).value_counts().sort_index()
                counter = 0
                c_df['chunk_group'] = 0
                for index,count in enumerate(index_range):
                    if index==0:
                        c_df['chunk_group'].loc[0:count]=index+1
                    else:
                        c_df['chunk_group'].loc[counter+1:count+counter]=index+1
                    
                    counter+=count
 
                c_df['chunk_group'][c_df['chunk_group']==0] = c_df['chunk_group'].max()+1
                grouped_chunk_df = c_df.groupby(c_df['chunk_group'])
                for k,g in grouped_chunk_df:
                    g['chunk_order'] = k # previously +1 was there and just because of that chunk group was stared from 2
                    g = g.drop('chunk_group',axis=1)
                    chunk_dfs.append(g)
            
    return chunk_dfs

def calculate_idle_time(df):
    from itertools import groupby, count
#     df = df.drop('level_0',axis=1)
    df.reset_index(inplace=True, drop= True)
    
    i = df[(df['vehicle_speed'] ==0) & (df['engine_speed']< 1000.00)].index
    total_idle_time=0
    if len(i)>0:
        idle_index =[]
        def as_range(g):
            l = list(g)
            return l[0], l[-1]

        idle_index =[as_range(g) for _, g in groupby(i, key=lambda n, c=count(): n-next(c))]
        for value in idle_index:
            total_idle_time+=(df['time_stamp'].iloc[value[1]] - df['time_stamp'].iloc[value[0]]).total_seconds()
            
    return total_idle_time

def get_engine_running_time(df):
    
#     df=df.drop('level_0',axis=1)
    df.reset_index(inplace=True, drop=True)
    df['time_diff'] = df['time_stamp'].diff()
    df['time_diff'] = df['time_diff']/np.timedelta64(1, 's')
    
    gap_list = df[df['time_diff']>300].index
    total_time=0
    if len(gap_list)>0:
        time_index_list = []
        for index in range(len(gap_list)):
            if index ==0:
                time_index_list.append((0,(gap_list[index]-1)))
            else:
                time_index_list.append((gap_list[index-1],(gap_list[index]-1)))

        time_index_list.append((gap_list[-1],df.index[-1]))

        for time_index in time_index_list:

            total_time+= (df['time_stamp'].iloc[time_index[1]] - df['time_stamp'].iloc[time_index[0]]).total_seconds()
    else:
        total_time = (df['time_stamp'].iloc[-1] - df['time_stamp'].iloc[0]).total_seconds()
        
    return total_time


def get_regen_totaltime_count(df):
    
    """
    Calculate total regen time and count for each chunk
    Returns Total Regen time and count
    """
    
    from itertools import groupby, count
    
    df.reset_index(inplace = True,drop=True)
    df['time_stamp'] = pd.to_datetime(df['time_stamp'])
    df.sort_values('time_stamp',inplace=True)
    values = df[df['regen_duration']!=0].index

    miss_list =[]
    def as_range(g):
        l = list(g)
        return l[0], l[-1]

    miss_list =[as_range(g) for _, g in groupby(values, key=lambda n, c=count(): n-next(c))]

#     regen_time = [df['regen_duration'].iloc[item[1]] for item in miss_list]
    # doesn't devide the regin time
    regen_time = [ df['regen_duration'].iloc[item[1]] -  df['regen_duration'].iloc[item[0]] for item in miss_list]
    regen_time = list(filter(lambda num: num != 0 and str(num)!='nan',regen_time))
        
    return sum(regen_time), len(regen_time)


def reject_outliers(sr, iq_range=0.5, side='left', return_mask=False):
    """
    Takes an array (or pandas series) and returns an array with outliers excluded, 
    according to the  interquartile range.
    Parameters:
    -----------
    sr: array
        array of numeric values
    iq_range: float
        percent to calculate quartiles by, 0.5 will yield 25% and 75%ile quartiles
    side: string
        if 'left', will return everything below the highest quartile
        if 'right', will return everything above the lowest quartile
        if 'both', will return everything between the high and low quartiles
    """
    pcnt = (1 - iq_range) / 2
    qlow, median, qhigh = sr.dropna().quantile([pcnt, 0.50, 1-pcnt])
    iqr = qhigh - qlow
    
    if side=='both':
        mask = (sr - median).abs() <= iqr
    elif side=='left':
        mask = (sr - median) <= iqr
    elif side=='right':
        mask = (sr - median) >= iqr
    else:
        print('options for side are left, right, or both')
    if return_mask:
        return mask
    return sr[mask]


import statistics
def get_grade_informations(df):
    """
    Calculates grade of road
    Not used in the final training and testing as this was not improving the 
    prediction
    """
    df.reset_index(inplace=True,drop=True)
    max_odometer = df['odometer_new'].iloc[-1]
    min_odometer = df['odometer_new'].iloc[0]
    max_alt = df['odometer_new'].iloc[-1]
    min_alt = df['odometer_new'].iloc[0]
    #grade a
    run = max_odometer - min_odometer
    rise = max_alt - min_alt
    
    grade_a = ((rise/1000)+run)*100
    
    #grade b
    list_dfs = create_chunk_by_km(df=df,km=5)
    list_grade = []
    
    for c_df in list_dfs:
        max_odometer = df['odometer_new'].iloc[-1]
        min_odometer = df['odometer_new'].iloc[0]
        max_alt = df['odometer_new'].iloc[-1]
        min_alt = df['odometer_new'].iloc[0]
        run = max_odometer - min_odometer
        rise = max_alt - min_alt
        grade = ((rise/1000)+run)*100
        list_grade.append(grade)
    if len(list_grade)>0:
        grade_b_min = min(list_grade)
        grade_b_max = max(list_grade)
        grade_b_median = statistics.median(list_grade)
    else:
        grade_b_min,grade_b_max,grade_b_median = np.nan,np.nan,np.nan
        
    return grade_a,grade_b_min,grade_b_max,grade_b_median


def filter_for_outliers(df,outlier_list):
#         df = df[df['engine_speed']>1000]
        for item in outlier_list:
            variable_name = item.get('variable_name')
            df = df[df[variable_name]>= item.get('outlier')[0]]
            df = df[df[variable_name]<= item.get('outlier')[1]]
        
        return df
    
def get_aggregated_chunk_data(df,outlier_list):
    
#     print("Calculating features on Cycle - {} & Chunk order {}".format(df['oil_cycle'].unique(),
#                                                                        df['chunk_order'].unique()))
    count_start = df[df['start_status']==1].shape[0]
    idle_time = calculate_idle_time(df)
    
    #removing the default values
    def conditional_filtering(df,engine_default =-0.12345):

        #removing the rows where Engine RPM is Default
        df = df[df['engine_speed'] != engine_default]
        # Removing the rows where Vehicle speed is Default
        df = df[df['vehicle_speed'] != engine_default]
        # Removing the rows where Vehicle odo readings is Default
        df = df[df['odometer'] != engine_default]

        df = df[(df['engine_speed'] != 0) & (df['vehicle_speed']!=0) ]
        return df
    
    df = conditional_filtering(df)
#     df = df.drop('level_0',axis=1)
    # Regen calculation 
    regen_total_time, no_of_regen = get_regen_totaltime_count(df)
    regen_total_time_new = df['regen_duration'][df['regen_duration']!=0].count()
    
    df.reset_index(inplace=True,drop=True)
    if df.shape[0]>1:
        distance = df['odometer'].iloc[-1] - df['odometer'].iloc[0]
        odo_start = df['odometer'].iloc[0]
        odo_end = df['odometer'].iloc[-1]
    #removing outliers
    df = filter_for_outliers(df,outlier_list)
    df['fuelreq_by_nt'] = df['fuelreq']/(df['engine_speed']*df['engine_load'])
    if df.shape[0]>1:
        x_df = df.loc[:,['ambient_temperature','engine_speed','altitude','baro_pressure','fuelreq','fuelreq_by_nt']]
        x_df = x_df.describe()
        x_df= x_df.reset_index().melt(id_vars="index")
        x_df = x_df[x_df['index'].isin(['min','mean','max','50%','std'])]
        x_df['variable']= x_df['variable']+"_"+x_df['index']
        x_df= x_df.drop("index",axis=1)
        x_df = x_df.T
        header = x_df.iloc[0]
        x_df= x_df[1:]
        x_df.columns = header
        x_df.reset_index(inplace=True,drop=True)
        #varibale wise feature extraction
        variable_list = ['vehicle_speed','engine_load','viscosity_until_serv','eng_oil_life_left',
                         'boost_pressure','dpf_presdrop','mass_air_flow','o2_conc','soot_level',
                         'exhaust_mass_flow']

        for var in variable_list:
            
            x_df[var+'_min'] = df[var][df[var]>0].min()
            x_df[var+'_max'] = df[var][df[var]>0].max()
            x_df[var+'_mean'] = df[var][df[var]>0].mean()
            x_df[var+'_std'] = df[var][df[var]>0].std()
            x_df[var+'_50%'] = df[var][df[var]>0].median()

        #no of trips
        x_df['no_of_trip'] = count_start

        var_list = ['vehicle_speed','engine_load','viscosity_until_serv','eng_oil_life_left',
                         'boost_pressure','dpf_presdrop','mass_air_flow','o2_conc','soot_level',
                         'exhaust_mass_flow','ambient_temperature','engine_speed','altitude','baro_pressure','fuelreq','engine_coolant_temp',
                    'oil_pressure','fuelreq_by_nt']
            
        for var in var_list:
            if var in list(df.columns):
                x_df[var+'_kurt'] = df[var].kurt(skipna = True)
                x_df[var+'_skew'] = df[var].skew(skipna = True)
            else:
                x_df[var+'_kurt'] = np.nan
                x_df[var+'_skew'] = np.nan
        
        #engine_coolant_temp
        x_df['engine_coolant_temp_min'] = df['engine_coolant_temp'].min()
        x_df['engine_coolant_temp_max'] = df['engine_coolant_temp'].max()
        x_df['engine_coolant_temp_mean'] = df['engine_coolant_temp'].mean()
        x_df['engine_coolant_temp_std'] = df['engine_coolant_temp'].std()
        x_df['engine_coolant_temp_50%'] = df['engine_coolant_temp'].median()
        
        #oil_pressure
        x_df['oil_pressure_min'] = df['oil_pressure'].min()
        x_df['oil_pressure_max'] = df['oil_pressure'].max()
        x_df['oil_pressure_mean'] = df['oil_pressure'].mean()
        x_df['oil_pressure_std'] = df['oil_pressure'].std()
        x_df['oil_pressure_50%'] = df['oil_pressure'].median()
        # Correlation between Oil pressure vs Engine RPM
        
        x_df['corr_engine_rpm_oil_pressure_spearman'] = df['oil_pressure'].corr(df['engine_speed'],method = 'spearman')
        x_df['corr_engine_rpm_oil_pressure_kendall'] = df['oil_pressure'].corr(df['engine_speed'],method = 'kendall')
        x_df['corr_engine_rpm_oil_pressure_pearson'] = df['oil_pressure'].corr(df['engine_speed'],method = 'pearson')
      
        

        #acceleration
        x_df['acceleration_min'] = df['acceleration-decceleration'][df['acceleration-decceleration']>0].min()
        x_df['acceleration_max'] = df['acceleration-decceleration'][df['acceleration-decceleration']>0].max()
        x_df['acceleration_mean'] = df['acceleration-decceleration'][df['acceleration-decceleration']>0].mean()
        x_df['acceleration_std'] = df['acceleration-decceleration'][df['acceleration-decceleration']>0].std()
        x_df['acceleration_50%'] = df['acceleration-decceleration'][df['acceleration-decceleration']>0].median()
        x_df['acceleration_kurt'] = df['acceleration-decceleration'][df['acceleration-decceleration']>0].kurt(skipna = True)
        x_df['acceleration_skew'] = df['acceleration-decceleration'][df['acceleration-decceleration']>0].skew(skipna = True)

        #decceleration

        x_df['decceleration_min'] = df['acceleration-decceleration'][df['acceleration-decceleration']<0].min()
        x_df['decceleration_max'] = df['acceleration-decceleration'][df['acceleration-decceleration']<0].max()
        x_df['decceleration_mean'] = df['acceleration-decceleration'][df['acceleration-decceleration']<0].mean()
        x_df['decceleration_std'] = df['acceleration-decceleration'][df['acceleration-decceleration']<0].std()
        x_df['decceleration_50%'] = df['acceleration-decceleration'][df['acceleration-decceleration']<0].median()
        x_df['decceleration_count'] = df['acceleration-decceleration'][df['acceleration-decceleration']<0].count()
        x_df['acceleration_kurt'] = df['acceleration-decceleration'][df['acceleration-decceleration']<0].kurt(skipna = True)
        x_df['acceleration_skew'] = df['acceleration-decceleration'][df['acceleration-decceleration']<0].skew(skipna = True)

        # grade information
        grade_a,grade_b_min,grade_b_max,grade_b_median = get_grade_informations(df)
        x_df['grade_a'] = grade_a
        x_df['grade_b_min'] = grade_b_min
        x_df['grade_b_max'] = grade_b_max
        x_df['grade_b_median'] = grade_b_median
        
        
        x_df['regen_total_time']=regen_total_time
        x_df['regen_total_time_new']=regen_total_time_new
        x_df['no_of_regen']=no_of_regen

        #Change in Viscosity
        x_df['viscosity_change'] = df['viscosity_until_serv'].iloc[0] - df['viscosity_until_serv'].iloc[-1]
        x_df['total_time_sec'] = get_engine_running_time(df)
        
        #cycle information
        x_df['oil_cycle']=df['oil_cycle'].iloc[0]
        x_df['chunk_order']=df['chunk_order'].iloc[0]
        # Total idle time 
        x_df['idle_time'] = idle_time
        x_df['distance'] = distance
        x_df['odo_start'] = odo_start
        x_df['odo_end'] = odo_end
        
        
        return x_df
    else:
        return np.nan

def get_outlier_information(data_dir):
    
    data_list = np.sort(np.array(glob.glob(data_dir+'*.parquet')))[::-1]
    oil_pressure_out_min = []
    oil_pressure_out_max = []
    engine_coolant_temp_out_min = []
    engine_coolant_temp_out_max = []
    engine_speed_out_min = []
    engine_speed_out_max = []
    
    for raw_data in data_list:
        
        df = pd.read_parquet(raw_data)
        #Removing default value from the data
        df = df[df['oil_pressure']!=-0.12345]
        df = df[df['engine_coolant_temp']!=-0.12345]
        df = df[df['engine_speed']!=-0.12345]
        
        oil_pressure = reject_outliers(df['oil_pressure'],side='both')
        
        engine_coolant_temp = reject_outliers(df['engine_coolant_temp'],side='both')
        
        engine_speed = reject_outliers(df['engine_speed'],side='both')
        
        oil_pressure_out_min.append(oil_pressure.min())
        oil_pressure_out_max.append(oil_pressure.max())
        
        engine_coolant_temp_out_min.append(engine_coolant_temp.min())
        engine_coolant_temp_out_max.append(engine_coolant_temp.max())
        
        engine_speed_out_min.append(engine_speed.min())
        engine_speed_out_max.append(engine_speed.max())
        
        del df
        del oil_pressure
        del engine_coolant_temp
        del engine_speed
    
    
    #outlies
    outlier_list=[]
    #oil_pressure
    outlier_list.append({'variable_name':'oil_pressure','outlier':(min(oil_pressure_out_min),
                                                                   max(oil_pressure_out_max))})

    # engine_coolant_temp
    outlier_list.append({'variable_name':'engine_coolant_temp','outlier':(min(engine_coolant_temp_out_min),
                                                                         max(engine_coolant_temp_out_max))})

    # engine_speed
    outlier_list.append({'variable_name':'engine_speed','outlier':(min(engine_speed_out_min),
                                                                    max(engine_speed_out_max))})
        
        
    return outlier_list


#def data_pipe_line(data_dir ='/home/deeptanils/Desktop/Navistar/src/raw_data/' ,
#                   output_dir='/home/deeptanils/Desktop/Navistar/src/aggregated_data_v23/'):

def data_aggregation(input_dir ='/home/deeptanils/Desktop/Navistar/src/raw_data/' ,
                   output_dir='/home/deeptanils/Desktop/Navistar/src/aggregated_data_v23/'):
    
    concat_data_list = np.sort(np.array(glob.glob1(input_dir,'*.parquet')))#[::-1]
#     data_list = [file_ for file_ in data_list if 'VIN_1059' in file_ ]
    agree_data_list = np.sort(np.array(glob.glob1(output_dir,'*.parquet')))[::-1]
    #agree_data_list = [data.replace(output_dir,'') for data in agree_data_list]
    
    
#     outlier_list = get_outlier_information(data_dir)
#     outlier_list[2]['outlier'] = (500,outlier_list[2].get('outlier')[1])
    outlier_list=[{'variable_name': 'oil_pressure', 'outlier': (112.0625, 390.375)},
                     {'variable_name': 'engine_coolant_temp', 'outlier': (79.0, 92.0)},
                     {'variable_name': 'engine_speed', 'outlier': (500, 2371.25)}]
    #    os.mkdir(output_dir)
    for raw_data in concat_data_list:
        #raw_data = concat_data_list[0]
        file_name = raw_data.replace(input_dir,'')
        print("Processing file : ", file_name)
        
        df = pd.read_parquet(input_dir+raw_data)
        print("Reading raw data completed")
        # Data preparation layer

        #Extraxt the cycle info from data
        df = extract_cycle_info_by_viscosity(df) 
        print("Cycle information extracted")
        #todo un comment below line
        df = prepare_data(df)
        print("Data prepared")
        # Create chunk dataframes and append in the list
        chunk_km = [30]  #,500
        for km in chunk_km:
            #km = 30
            if (file_name[0:8]+'_'+str(km)+'_aggregated_data.csv') not in agree_data_list:
                print("Creating chunk of ",str(km))
                list_df= create_chunk_by_km(df,km=km)
                print("Chunk creation completed. ",str(km))
                
                #aggregate the data for each chunk
                print("Data aggregation started for file : ",file_name)
                aggregated_df = [get_aggregated_chunk_data(df,outlier_list) for df in list_df]
                aggregated_df =  [ df for df in aggregated_df if str(df) !='nan']
                aggregated_df = pd.concat(aggregated_df)
                aggregated_df['chunk_size_km'] = km
                # added later to include VIN Information
                aggregated_df['VIN'] = raw_data[:8]
                aggregated_df.reset_index(inplace=True)
                print("Data aggregation completed for file : ",file_name)
#                aggregated_df.to_hdf(output_dir+ file_name[0:8]+'_'+str(km)+'_aggregated_data.h5',key='stage', mode='w')
                aggregated_df.to_csv(output_dir+ file_name[0:8]+'_'+str(km)+'_aggregated_data.csv', index=False)
                del list_df
                del aggregated_df
            else:
                print("Data already processed for file: ",(file_name[0:8]+'_'+str(km)+'_aggregated_data.csv'))
        
        del df


if __name__ == "__main__":
    #input_dir = '../output/concatenated_data/' #contains concatenated data
    #output_dir = '../output/aggregated_data/'  #contains aggregated data
    input_dir = sys.argv[1]  #concatenated data dir
    output_dir = sys.argv[2] #aggregated data dir
    data_aggregation(input_dir,output_dir)   

# python 3_data_aggregation.py '../output/concatenated_data/' '../output/aggregated_data/'

#import time
#start_time = time.time()
#data_aggregation(input_dir = '../output/concatenated_data/', output_dir = '../output/aggregated_data/')   
#end_time = time.time()
#total_time = end_time - start_time
#print(total_time)
#print("Execution Time: %s seconds" % (time.time() - start_time)) 

#Execution Time: 470.2938325405121 seconds (~8 minutes)