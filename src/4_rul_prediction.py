# -*- coding: utf-8 -*-
"""
Created on Mon Jul 27 13:44:57 2020

@author: mohammads6
"""
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import h2o
h2o.init()
from h2o.estimators.gbm import H2OGradientBoostingEstimator
from h2o.estimators.random_forest import H2ORandomForestEstimator
from h2o.estimators import H2OXGBoostEstimator
from sklearn import linear_model

from sklearn.preprocessing import MinMaxScaler
from sklearn import metrics
from sklearn.metrics import r2_score, mean_absolute_error

def prepare_model_data(df, chunk_size_km):
    """
    Add additional features like oil_life, rul_distance, distance_cumsum,
    regen_total_time_cumsum, total_time_sec_cumsum, idle_time_cumsum
    """

    list_df = []
    vins = df['VIN'].unique()
    for vin in vins:
        v_df = df[df['VIN']==vin]
        v_df = v_df.reset_index(drop=True)
        cycles = v_df['oil_cycle'].unique()
        for cycle in cycles:

            c_df = v_df[v_df['oil_cycle']==cycle]
            c_df = c_df.reset_index(drop=True)
            max_order = chunk_size_km*c_df['chunk_order'].max()
            c_df['oil_life'] = (max_order - c_df['chunk_order']*chunk_size_km)*100/max_order 
            #print()
            c_df['rul_distance'] = (max_order - c_df['chunk_order']*chunk_size_km)
            c_df['distance_cumsum'] = c_df['distance'].cumsum()
            #regen_total_time
            c_df['regen_total_time_cumsum'] = c_df['regen_total_time_new'].cumsum()
            #total_time_sec
            c_df['total_time_sec_cumsum'] = c_df['total_time_sec'].cumsum()
            #idle_time
            c_df['idle_time_cumsum'] = c_df['idle_time'].cumsum()
            list_df.append(c_df)

    return pd.concat(list_df)


def extract_valid_cycle(df):
    """
    Extract only the valid cycles from the data and remove the rest
    """

    cycle_df = pd.read_csv('cycle_validation_information.csv')
    cycle_df['odometer_missing'] = cycle_df['odometer_missing'].abs() 
    invalid_cycle_one = cycle_df[((cycle_df['viscosity_start']<95) | (cycle_df['viscosity_end']>15))]
    invalid_cycle_two = cycle_df[cycle_df['odometer_missing']>30]
    invalid_cycle = pd.concat([invalid_cycle_one,invalid_cycle_two])
    invalid_cycle.drop_duplicates(inplace=True)
    
    df['filter'] = df['VIN'] + '_' + df['oil_cycle'].astype(str)
    print(invalid_cycle.shape)
    for index,row in invalid_cycle.iterrows():
        value = row['VIN'] + '_' + str(row['oil_cycle'])
        df = df[df['filter']!=value ]    
    return df

def create_temparatre_catagories(temp):
    """
    Add temperature bins to the data
    """
    if 50 >= temp > 25:
            return 1
    elif 25 >= temp > 0:
        return 2
    elif 0 >= temp > -25:
        return 3
    elif -25 >= temp > -50:
        return 4
    
def create_pressure_catagories(pressure):
    """
    Add Pressure Bins
    """
    
    if  pressure<= 70 and pressure > 60:
        return 1
    elif pressure <= 80 and pressure> 70:
        return 2
    elif pressure <= 90 and pressure > 80:
        return 3
    elif pressure <= 100 and pressure > 90:
        return 4
    elif pressure <= 110 and pressure > 100:
        return 5

def regen_active(value):
    """
    Add details if the regeneration was active or not
    """
    if value!=0:
        return 1
    else:
        return 0
    
def create_oil_life_categories(oil_life):
    
    # if 100 >= oil_life > 75:
    #         return 4
    # elif 75 >= oil_life > 50:
    #     return 3
    # elif 50 >= oil_life > 25:
    #     return 2
    # elif 25 >= oil_life >= 0:
    #     return 1
  if 100 >= oil_life > 25:
    return 0
  elif 25 >= oil_life >= 0:
    return 1    

def create_viscosity_categories(viscosity):

  if 100 >= viscosity > 25:
    return 0
  elif 25 >= viscosity >= 0:
    return 1

#def prepare_initial_data(file_path='/home/chaitra/Projects/Navistar/total_aggregated_data_v4.csv',chunk_size_km = 30):
def prepare_initial_data(path = "../output/aggregated_data/", file_name="VIN_1039_30_aggregated_data.csv",chunk_size_km = 30):
    file_path = path+file_name
    print(file_path)
    df = pd.read_csv(file_path)
    df.columns
    df.drop('index',axis=1,inplace=True)
    df = df.reset_index(drop=True)
    df.drop(['altitude_kurt','altitude_skew','altitude_mean','altitude_std',
       'altitude_min','altitude_50%','altitude_max','exhaust_mass_flow_min','exhaust_mass_flow_std','exhaust_mass_flow_50%',
       'exhaust_mass_flow_mean','exhaust_mass_flow_max','exhaust_mass_flow_kurt','exhaust_mass_flow_skew'],axis=1,inplace=True)
    ##'soot_level_min', 'soot_level_max','soot_level_mean', 'soot_level_std', 'soot_level_50%','soot_level_kurt','soot_level_skew',

    #Doing modeling on chunk size 100
    df = df[df['chunk_size_km'] == chunk_size_km]
    df = prepare_model_data(df, chunk_size_km)
    
    print(df.shape)
    #df = extract_valid_cycle(df)
    print(df.shape)
    # df.dropna(inplace=True)
    df.fillna(method='ffill',inplace=True)
    print(df.shape)
    # print(df['ambient_temperature_mean'].min())
    # print(df['ambient_temperature_mean'].max())
    #df['regen_active'] = df['regen_total_time_new'].apply(regen_active)
    df['ambient_temperature_bin'] = df['ambient_temperature_mean'].apply(create_temparatre_catagories)
    #Adding Pressure Bins
    #df['baro_pressure_bin'] = df['baro_pressure_mean'].apply(create_pressure_catagories)
    df['oil_life_categories'] = df['oil_life'].apply(create_oil_life_categories)
    #df['viscosity_categories'] = df['viscosity_until_serv_50%'].apply(create_viscosity_categories)
    return df

def add_lag_to_dataset(df,order=1,drop=['VIN','oil_cycle']):
    print("Adding lag to dataset")
    vins = df['VIN'].unique()
    lag_df_list=[]
    for vin in vins:
      v_df= df[df['VIN']==vin]
      cycles = v_df['oil_cycle'].unique()
      for cycle in cycles:
          c_df = v_df[v_df['oil_cycle']==cycle]
          c_df.drop(drop,inplace=True,axis=1)
          x_df = c_df.shift(axis=order,fill_value=0)
          x_df.columns = [data+'_lag' for data in list(x_df.columns)]
          lag_df_list.append(x_df)

    df.drop(drop,inplace=True,axis=1)
    lag_df = pd.concat(lag_df_list,axis=0)
    df = pd.concat([df,lag_df],axis=1)
    return df
    
#    required_cols = ['ambient_temperature_std','ambient_temperature_min','ambient_temperature_50%',
#                     'engine_speed_std','engine_speed_min','engine_speed_50%',
#                     'baro_pressure_std','baro_pressure_min','baro_pressure_50%',
#                #'fuelreq_std','fuelreq_min','fuelreq_50%','vehicle_speed_min','vehicle_speed_std','vehicle_speed_50%',
#                'engine_load_min','engine_load_std','engine_load_50%',
#                #'boost_pressure_min','boost_pressure_std','boost_pressure_50%','dpf_presdrop_min','dpf_presdrop_std','dpf_presdrop_50%',
#                #'mass_air_flow_min','mass_air_flow_std','mass_air_flow_50%','vehicle_speed_kurt','vehicle_speed_skew','dpf_presdrop_kurt','dpf_presdrop_skew',
#                'o2_conc_min','o2_conc_std','o2_conc_50%',
#                'no_of_trip','engine_load_kurt','engine_load_skew',
#                #'boost_pressure_kurt','boost_pressure_skew',
#                #'mass_air_flow_kurt','mass_air_flow_skew',
#                'o2_conc_kurt','o2_conc_skew','ambient_temperature_kurt','ambient_temperature_skew',
#                'engine_speed_kurt','engine_speed_skew','baro_pressure_kurt','baro_pressure_skew',#'fuelreq_kurt','fuelreq_skew',
#                'engine_coolant_temp_kurt','engine_coolant_temp_skew','oil_pressure_kurt','oil_pressure_skew',
#                'engine_coolant_temp_min',
#                'engine_coolant_temp_std','engine_coolant_temp_50%','oil_pressure_min','oil_pressure_std','oil_pressure_50%',
#                'corr_engine_rpm_oil_pressure_pearson',
#                'chunk_order','regen_total_time_new','total_time_sec_cumsum','regen_total_time_cumsum'
#                ]
    
def prepare_train_data(df, target_column, required_cols):
    #target_column = "viscosity_until_serv_50%"
    X = df[required_cols]

    vins = df['VIN']
    oil_cycle = df['oil_cycle']
    ambient_temperature_bin = df['ambient_temperature_bin']
    #baro_pressure_bin = df['baro_pressure_bin']
    #regen_active= df['regen_active']
    
    scaler = MinMaxScaler()
    X_norm = scaler.fit_transform(X)
    X = pd.DataFrame(X_norm,columns=X.columns)
    X['VIN'] = vins.values
    X['oil_cycle']=oil_cycle.values
    X['ambient_temperature_bin'] = ambient_temperature_bin.values
    #X['baro_pressure_bin'] = baro_pressure_bin.values
    #X['regen_active'] = regen_active.values
    X = add_lag_to_dataset(X)
    Y = df[target_column]
    train = X
    train[target_column] = Y.values
    train = h2o.H2OFrame(train)
    covtype_X = train.col_names
    covtype_X.remove(target_column)
    covtype_y = target_column
    train['ambient_temperature_bin'] = ((train['ambient_temperature_bin']).ascharacter()).asfactor()
    train['ambient_temperature_bin_lag'] = ((train['ambient_temperature_bin_lag']).ascharacter()).asfactor()
    
    #Add Baro Pressure Bin
    #train['baro_pressure_bin'] = ((train['baro_pressure_bin']).ascharacter()).asfactor()
    #train['baro_pressure_bin_lag'] = ((train['baro_pressure_bin_lag']).ascharacter()).asfactor()

    #train['regen_active'] = ((train['regen_active']).ascharacter()).asfactor()
    #train['regen_active_lag'] = ((train['regen_active_lag']).ascharacter()).asfactor()
    return train,X,covtype_X,covtype_y,Y, scaler

    # =============================================================================
    # Prepare test Data by subseting on required data and scaling
    # Input: Test Data Frame, list of required columns
    # Output: test(h2o dataframe), x, oil_cycle    
    # =============================================================================
def prepare_test_data(df,scaler,required_cols):
 
    x = df[required_cols]
    vins = df['VIN']
    ambient_temperature_bin = df['ambient_temperature_bin']
    #baro_pressure_bin = df['baro_pressure_bin']

    #regen_active= df['regen_active']
    oil_cycle = df['oil_cycle']
    # input(1)
    x_norm = scaler.transform(x)
    # input(1)
    x = pd.DataFrame(x_norm,columns=x.columns)
    # input(2)
    x['VIN'] = vins.values
    x['oil_cycle']=oil_cycle.values
    x['ambient_temperature_bin'] = ambient_temperature_bin.values
    #x['baro_pressure_bin'] = baro_pressure_bin.values
    #x['regen_active'] = regen_active.values
    x = add_lag_to_dataset(x)
    test = x.copy()
    test = h2o.H2OFrame(test)
    test['ambient_temperature_bin'] = ((test['ambient_temperature_bin']).ascharacter()).asfactor()
    test['ambient_temperature_bin_lag'] = ((test['ambient_temperature_bin_lag']).ascharacter()).asfactor()
    #Adding barometric pressure
    #test['baro_pressure_bin'] = ((test['baro_pressure_bin']).ascharacter()).asfactor()
    #test['baro_pressure_bin_lag'] = ((test['baro_pressure_bin_lag']).ascharacter()).asfactor()
    #test['regen_active'] = ((test['regen_active']).ascharacter()).asfactor()
    #test['regen_active_lag'] = ((test['regen_active_lag']).ascharacter()).asfactor()
    return test,x,oil_cycle
    
    # Testing prepare_test_dat
    # test_df, x_df, oil_cycle = prepare_test_data(df_test,scaler,required_cols)    
    # df_test
    #df['oil_life'] = df['oil_life'].values
    #df['oil_cycle'] = list(oil_cycle)
    #x['chunk_order_new']= df['chunk_order'].values    
    #df['viscosity_predicted_xgb'] = xgb_model_viscosity.predict(test).as_data_frame()    
    #df['viscosity_predicted_rf'] = rf_model_viscosity.predict(test).as_data_frame()    
    
def train_regression_model(df1, target_column , required_cols):
    train,X,covtype_X,covtype_y,Y, scaler = prepare_train_data(df1,target_column, required_cols)
    
    # Neural Network  Model
#    algorithm_name = 'nn'
#    model_name = "navistar_rul_" + algorithm_name + "_" + target_column + "_" + model_version
#
#    nn_model = H2ODeepLearningEstimator(
#        model_id="nn_model_navistar_rul",
#        distribution="tweedie",
#        hidden=[1],
#        epochs=1000,
#        train_samples_per_iteration=-1,
#        reproducible=True,
#        activation="Tanh",
#        single_node_mode=False,
#        balance_classes=False,
#        force_load_balance=False,
#        seed=23123,
#        tweedie_power=1.5,
#        score_training_samples=0,
#        score_validation_samples=0,
#        stopping_rounds=0,
#        input_dropout_ratio= 0,
#        export_weights_and_biases=True)
#    nn_model.train(covtype_X, covtype_y, training_frame=train)#, validation_frame=valid)
#    print("Neural Neywork Model Training result R2 = {} and MAE = {}".format(nn_model.r2(),nn_model.mae()))
    
    model_version = "V1"
    #XGB Model
    algorithm_name = 'xgb'
    model_name = "navistar_rul_" + algorithm_name + "_" + target_column + "_" + model_version
    param = {
      "ntrees" : 200#10
      , "max_depth" : 10
      , "learn_rate" : 0.05 # 0.05
      , "sample_rate" : 0.7
      , "col_sample_rate_per_tree" : 0.9
      , "min_rows" : 5
      , "seed": 4241
      , "score_tree_interval": 10
      }
    xgb_model = H2OXGBoostEstimator(**param)
    xgb_model.train(covtype_X, covtype_y, training_frame = train)
    print("XGBoost Model Training result R2 = {} and MAE = {}".format(xgb_model.r2(),xgb_model.mae()))

      # Random Forest Model
    algorithm_name = 'rf'
    model_name = "navistar_rul_" + algorithm_name + "_" + target_column + "_" + model_version
    #if model_name not in directory_list:
    rf_model = H2ORandomForestEstimator(
        model_id="rf_model_navistar_rul",
        ntrees=200,
        max_depth=20,
        stopping_rounds=2,
        stopping_tolerance=0.01,
        score_each_iteration=True,
        seed=4241)
    rf_model.train(covtype_X, covtype_y, training_frame=train)#, validation_frame=valid)
    print("RandomForest Model Training result R2 = {} and MAE = {}".format(rf_model.r2(),rf_model.mae()))    
#    from sklearn.neural_network import MLPRegressor
#    nn_sklearn = MLPRegressor(hidden_layer_sizes=(16,16),activation='relu',random_state=23123)
#    nn_sklearn.fit(X,Y)
  # def save_model(model,model_path,algorithm_name,target_column,test_vin):
#   save_model(rf_model,model_path,algorithm_name,target_column,model_version)
    #nn_model,
    
    return xgb_model, rf_model, scaler
    
#    input_dir = "../output/aggregated_data/"
#    file_name = "VIN_1039_30_aggregated_data.csv"
#    
#    df = prepare_initial_data(path = "../output/aggregated_data/",file_name='VIN_1039_30_aggregated_data.csv')
#    df['VIN_oil_cycle'] = df['VIN'] + '_' + df['oil_cycle'].astype(str)
#    
#    test_oil_cycle = "VIN_1039_2"
#    
#    df_train = df[~(df["VIN_oil_cycle"]==test_oil_cycle)]
#    df_test = df[df["VIN_oil_cycle"]==test_oil_cycle]
#    
#    df_test.shape
#    
#    # Reading and Preparing training data
#    target_column = "viscosity_until_serv_50%"
#    train,X,covtype_X,covtype_y,Y, scaler = prepare_train_data(df_train, target_column, required_cols)
#    
#    xgb_model, rf_model, scaler = train_regression_model(df_train, target_column, required_cols)
    
def run_prediction(input_dir = "../output/aggregated_data/",
                   output_dir = "../output/prediction/",
               agg_file_name = "VIN_1039_30_aggregated_data.csv",
               test_oil_cycle = 'VIN_1039_2'):  
    
    #input_dir = "../output/aggregated_data/"
    #file_name = "VIN_1039_30_aggregated_data.csv"
    
    required_cols = ['ambient_temperature_std','ambient_temperature_min','ambient_temperature_50%',
         'engine_speed_std','engine_speed_min','engine_speed_50%',
         'baro_pressure_std','baro_pressure_min','baro_pressure_50%',
    #'fuelreq_std','fuelreq_min','fuelreq_50%','vehicle_speed_min','vehicle_speed_std','vehicle_speed_50%',
    'engine_load_min','engine_load_std','engine_load_50%',
    #'boost_pressure_min','boost_pressure_std','boost_pressure_50%','dpf_presdrop_min','dpf_presdrop_std','dpf_presdrop_50%',
    #'mass_air_flow_min','mass_air_flow_std','mass_air_flow_50%','vehicle_speed_kurt','vehicle_speed_skew','dpf_presdrop_kurt','dpf_presdrop_skew',
    'o2_conc_min','o2_conc_std','o2_conc_50%',
    'no_of_trip','engine_load_kurt','engine_load_skew',
    #'boost_pressure_kurt','boost_pressure_skew',
    #'mass_air_flow_kurt','mass_air_flow_skew',
    'o2_conc_kurt','o2_conc_skew','ambient_temperature_kurt','ambient_temperature_skew',
    'engine_speed_kurt','engine_speed_skew','baro_pressure_kurt','baro_pressure_skew',#'fuelreq_kurt','fuelreq_skew',
    'engine_coolant_temp_kurt','engine_coolant_temp_skew','oil_pressure_kurt','oil_pressure_skew',
    'engine_coolant_temp_min','engine_coolant_temp_std','engine_coolant_temp_50%',
    'oil_pressure_min','oil_pressure_std','oil_pressure_50%',
    'corr_engine_rpm_oil_pressure_pearson',
    'chunk_order','regen_total_time_new','total_time_sec_cumsum','regen_total_time_cumsum'
    ]

    df = prepare_initial_data(path = "../output/aggregated_data/",file_name='VIN_1039_30_aggregated_data.csv')
    df['VIN_oil_cycle'] = df['VIN'] + '_' + df['oil_cycle'].astype(str)
    
    #test_oil_cycle = "VIN_1039_2"
    
    df_train = df[~(df["VIN_oil_cycle"]==test_oil_cycle)]
    df_test = df[df["VIN_oil_cycle"]==test_oil_cycle]
    
    target_column = 'viscosity_until_serv_50%'
    xgb_model, rf_model, scaler = train_regression_model(df_train, target_column , required_cols)
    
    test,x,oil_cycle = prepare_test_data(df_test,scaler,required_cols)
    df_test['viscosity_predicted_xgb'] = xgb_model.predict(test).as_data_frame()
    df_test['viscosity_predicted_rf'] = rf_model.predict(test).as_data_frame()
    
    #save the preicitions
    df_test.to_csv(output_dir+test_oil_cycle+"_prediction.csv", index=False)
    
    fig = df_test.plot("chunk_order", ["viscosity_until_serv_50%","viscosity_predicted_xgb"], style='.-').get_figure()
    plt.xlabel('chunk_order')
    plt.ylabel('viscosity_predicted')
    plt.title("Prediction for : " + test_oil_cycle)
    plt.title('Predicted Viscosity   VS   Chunk Order')
    plt.ylim(0,110)
    fig.savefig(output_dir+'{}_prediction_plot.png'.format(test_oil_cycle))


if __name__ == "__main__":
    input_dir = sys.argv[1]        #aggregated data folder
    output_dir = sys.argv[2]        # prediction output data folder
    agg_file_name = sys.argv[3]     # name of aggregated file
    test_oil_cycle=  sys.argv[4]    # test_oil_cycle to predict on
    
    run_prediction(input_dir = input_dir,           # "../output/aggregated_data/",
                   output_dir = output_dir,         # "../output/prediction_data/"
                   agg_file_name = agg_file_name,   #"VIN_1039_30_aggregated_data.csv",
                   test_oil_cycle = test_oil_cycle) #'VIN_1039_3') 

# python 4_rul_prediction.py "../output/aggregated_data/" "../output/predicted_data/" "VIN_1039_30_aggregated_data.csv",  'VIN_1039_4'


#    run_prediction(input_dir = "../output/aggregated_data/",
#                       output_dir = "../output/prediction/",
#                   agg_file_name = "VIN_1039_30_aggregated_data.csv",
#                   test_oil_cycle = 'VIN_1039_3') 