# -*- coding: utf-8 -*-
"""
Created on Wed Jul 22 19:42:03 2020

@author: mohammads6
"""
# https://anaconda.org/conda-forge/pyarrow
#conda install -c conda-forge pyarrow==0.15.1
# pyarrow required to save file in parquet format
# '0.15.1'
import sys
import numpy as np
import pandas as pd
import zipfile
import glob
#import pyarrow
#import os
#os.chdir('D:\\Drive\\projects\\Navistar\\Cloud Execution Pipeline\\scr')
#import os 
#from zipfile import ZipFile

# =============================================================================
# 1.0.1  Extract Zip file to CSV¶
# 1.0.2  Convert the columns name into specific format
# 1.0.3  Manage the continuous and categorical variables seperately
# 1.0.4  Concat and create a single H5 file
# =============================================================================

def read_from_zip(zip_file_path, input_dir):
    zf = zipfile.ZipFile(zip_file_path)
    dfs = []
    # Change Column Names
    df_rename = pd.read_csv(input_dir+"rename_columns.csv")
    df_rename = df_rename[["Original_Variables","New_Variables"]]
    column_rename_dict = {row[0]: row[1] for row in df_rename.values}
    for file_name in zf.namelist():
        if ".csv" in file_name:
            df = pd.read_csv(zf.open(file_name))
            df.columns = pd.Series(df.columns).replace(column_rename_dict)
            df.columns = pd.Series(df.columns).replace(column_rename_dict)

            #Split into "numeric" and  "object"
            numeric_df = df.select_dtypes(include="number")#.groupby(level=0, axis=1)
            object_df  = df.select_dtypes(exclude="number")#.groupby(level=0, axis=1)

            #Take a mean of the columns having the same name
            numeric_df = numeric_df.groupby(level=0, axis=1).mean()

            df = pd.concat([object_df, numeric_df], axis=1)
            del object_df
            del numeric_df
            dfs.append(df)

    df = pd.concat(dfs)
    print("Concatenated the Data")
    return df

def convert_zip_to_csv(zip_file_path,input_dir='', output_dir=''):
    # Set up the list of zip files to be opened and converted
    #Check the names of the parquet files present in the outpt directory
    csv_list = np.sort(np.array(glob.glob1(output_dir,'*.parquet')))[::-1]
    csv_list = [csvs.replace(output_dir,'')[0:8] for csvs in csv_list]
#     csv_list.append('VIN_4930')
    file_name = zip_file_path.replace(input_dir,'')
    print('File Name :',file_name[0:8]) 
    if file_name[0:8] not in csv_list:
        
        df = read_from_zip(zip_file_path, input_dir)
        df.to_parquet(output_dir+ file_name[0:8]+'_total_data.parquet')
        print(file_name[0:8]+ 'Created!')
        print("Removing data - Start")
        del df
        print("Removing data - End")


    else:
        print('File already processed- {}'.format(file_name[0:8]))
        
#zip_dir = 'D:/Drive/projects/Navistar/Cloud Execution Pipeline/'
#zip_dir = '../input/'        
#zip_list = np.sort(np.array(glob.glob(zip_dir+'*.zip')))[::-1]
#output_dir = '../output/'
##input(zip_list)
#for zip_file_path in zip_list:
#    #zip_file_path = zip_list[0]
#    convert_zip_to_csv(zip_file_path)        

def concatenate_data(input_dir ='../input/',output_dir= '../output/concatenated_data/'):
    #input_dir = '../input/'        
    #output_dir = '../output/'
    #Find the zip files inside the input directory
    zip_list = np.sort(np.array(glob.glob1(input_dir,'*.zip')))[::-1]
    zip_list = [input_dir+zip_files for zip_files in zip_list]
    
    #input(zip_list)
    for zip_file_path in zip_list:
        #zip_file_path = zip_list[0]
        convert_zip_to_csv(zip_file_path, input_dir, output_dir)   

if __name__ == "__main__":
    #input_dir ='../input/'
    #output_dir= '../output/concatenated_data/' #concatenated_data folder
    input_dir = sys.argv[1]  #input folder with zipped files
    output_dir = sys.argv[2] #concatenated_data folder

    concatenate_data(input_dir,output_dir)

# python 1_data_concatenation.py '../input/' '../output/concatenated_data/'
    
    
#import time
#start_time = time.time()
## Function to Run
#concatenate_data(input_dir,output_dir)
#end_time = time.time()
#total_time = end_time - start_time
#print(total_time)
#print("Execution Time for Data Consolidation: %s seconds" % (time.time() - start_time)) 
