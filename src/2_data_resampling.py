# -*- coding: utf-8 -*-
"""
Created on Thu Jul 30 00:14:47 2020

@author: mohammads6
"""
import sys
import pandas as pd
import numpy as np
import os
import warnings

def extract_cycle_info_by_viscosity(df):
    
    #Changing the column name as sorting the data based on given timestamp
    df['time_stamp'] = pd.to_datetime(df['time_stamp'])
    df.sort_values('time_stamp',inplace=True)
    df.reset_index(inplace=True)
    
    from itertools import groupby, count
    oil_cycle = []
    df['oil_cycle'] = 0
    cycle_index = df[df['viscosity_until_serv']>99].index
    cycle=[]
    
    for index in range(len(cycle_index)):
        if index==0:
            cycle.append(cycle_index[index])
        else:
            if cycle_index[index] -cycle_index[index-1]>50000:
                cycle.append(cycle_index[index])
    count =1
    for index in range(len(cycle)):
        if index ==0:
            df['oil_cycle'].iloc[0:cycle[index]] = count
        else:
            df['oil_cycle'].iloc[cycle[index-1]:cycle[index]] = count

        count+=1    
    
    df['oil_cycle'][df['oil_cycle']==0] = df['oil_cycle'].max()+1
    if len(df[df['oil_cycle']==1])<20000:
        df['oil_cycle'][df['oil_cycle']==1]=2
        df['oil_cycle']= df['oil_cycle']-1
        
    return df

#Lat Long Conversion
def nmea_to_degrees(series):
    degrees = series.str.extract("(\d{2,3})\d\d\.").astype(float)
    minutes = series.str.extract("(\d\d)\.").astype(float)
    seconds = series.str.extract("\.(\d\d)").astype(float)
    coordinates = degrees + minutes/60 + seconds/3600
    return coordinates
    
def data_resampling(input_dir = "../output/concatenated_data/",
                    output_dir = "../output/resampled_data/"):
    all_vin_data = pd.DataFrame()
    for vin in os.listdir(input_dir):
        #vin = 'VIN_1039_total_data.parquet'
        VIN_df = pd.read_parquet(input_dir+vin)
        VIN_df = extract_cycle_info_by_viscosity(VIN_df)
        VIN_df = VIN_df.set_index('time_stamp')
        VIN_df = VIN_df.resample("2min").first()
        VIN_df = VIN_df.dropna(axis=0, how="all")        
        VIN_df = VIN_df.replace([-.12345,-0.12345,'-0.12345'], np.nan)
        VIN_df = VIN_df[~(VIN_df['viscosity_until_serv']==0)]
        
        VIN_df['latitude_raw'] = VIN_df['latitude_raw'].astype(str)
        VIN_df['longitude_raw'] = VIN_df['longitude_raw'].astype(str)
        VIN_df['latitude'] = nmea_to_degrees(VIN_df['latitude_raw'])
        VIN_df['longitude'] = -nmea_to_degrees(VIN_df['longitude_raw'])
        #print("Converting Lat/Long fro NMEA format to Degrees")
        #VIN_df_oil_cycle = VIN_df[['oil_cycle','latitude','longitude']]
        #VIN_df = VIN_df[VIN_df.columns[~VIN_df.columns.isin(['oil_cycle','latitude','longitude'])]]
        #VIN_df_resample = VIN_df.resample("1min").mean()
        #VIN_df_oil_cycle = VIN_df_oil_cycle.resample("1min").first()
        #VIN_df_resample = VIN_df.resample("2min").first()
        #VIN_df_oil_cycle = VIN_df_oil_cycle.resample("1min").first()
    
        #VIN_df_all = pd.concat([VIN_df_resample,VIN_df_oil_cycle],axis=1)

        VIN_df = VIN_df.reset_index()
        VIN_df['oil_cycle'] = VIN_df['oil_cycle'].astype(int)
        VIN_df['VIN'] = vin[0:8]#.rstrip('_total_data.parquet')
        VIN_df['VIN_oil_cycle'] = VIN_df['VIN'] + '_' + VIN_df['oil_cycle'].astype(str)
        #VIN_df_all.to_csv('resampledata_1min/'+vin.rstrip('_total_data.parquet')+"_"+"resample.csv")
        #all_vin_data = all_vin_data.append(VIN_df_all, ignore_index=True)
        #del VIN_df
        #del VIN_df_resample
        #del VIN_df_all
        #del VIN_df_oil_cycle
    all_vin_data = pd.concat([all_vin_data, VIN_df], axis=0)
    del VIN_df
    all_vin_data.to_csv(output_dir+"resample_data.csv", index=False)


if __name__ == "__main__":
    #input_dir = "../output/concatenated_data/"
    #output_dir = "../output/resampled_data/"
    
    input_dir = sys.argv[1]
    output_dir = sys.argv[2]
    
    data_resampling(input_dir = "../output/concatenated_data/",
                        output_dir = "../output/resampled_data/")    
    
    #python 2_data_resampling.py "../output/concatenated_data/" "../output/resampled_data/"   