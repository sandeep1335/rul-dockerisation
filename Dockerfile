FROM ubuntu:18.04

ENV CONF_HOSTS 127.0.0.1
ENV DEBIAN_FRONTEND=noninteractive 

    
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        make gcc awscli \
        python3.7 python3.7-dev \
        python3-pip python3-wheel openjdk-8-jre-headless && \
    python3.7 -m pip install setuptools && \
    addgroup kpitgroup && \
    useradd -m  kpit -s /bin/bash -g kpitgroup && \
    echo 'kpit:kpitpass' | chpasswd && \
    mkdir /home/kpit/rul && \
    rm -r /root/.cache/pip && \
    apt-get remove -y --purge cpp cpp-7 linux-firmware libreoffice-core libreoffice-common libasan4 libgdbm5 openjdk-9-jre-headless  && \
    apt-get autoremove -y && \
    apt-get clean -y && \
    rm -rf /var/lib/apt/lists/*


# # copy sources
COPY ./src /home/kpit/rul

RUN mkdir input && mkdir output

COPY ./output /home/kpit/output

WORKDIR "/home/kpit/rul"
RUN chmod -R +x /home/kpit/rul/entry_point.sh && \
    chown -R kpit:kpitgroup /home/kpit && \
    python3.7 -m pip install -r requirements.txt && \
    rm -r /root/.cache/pip
 
# expose the working directory, ports etc.
USER kpit
ENTRYPOINT ["sh", "/home/kpit/rul/entry_point.sh"]

CMD []

# docker run -v /home/kpit/Desktop/rul/navister/dockerizing-rul/input:/input  -v /home/kpit/Desktop/rul/navister/dockerizing-rul/output:/output rul:1.0`



